<?php 

	require_once('Connection.php');

    if (!$admin->isLoggedIn()) {
        $admin->redirect('index.php');
    }

	if (isset($_POST['logout'])) {
		$admin->logout();
		$admin->redirect('index.php');
	}

	if (isset($_POST['delete'])) {
		$id = $_GET['id'];
		$admin->removeUser($id);
	}

 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Messaging App | User Details</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<div class="container col-lg-10 m-auto">
		<br><br>
		<h2 class="text-warning text-center">User Details</h2>		
		<form method="POST">
			<table class="table table-striped table-hover table-bordered text-center">
				<thead class="bg-dark text-white">
					<tr>
						<td>Name</td>
						<td>Username</td>
						<td>Email</td>
						<td>Action</td>
					</tr>
				</thead>
				<?php
					require_once('Connection.php');
					$result = $admin->seeAllUsers();

					foreach ($result as $value) {
				?>	
				<tbody>
					<tr>
						<td><?php echo $value['name']; ?></td>
						<td><?php echo $value['username']; ?></td>
						<td><?php echo $value['email']; ?></td>
						<td><a href="allUsers.php?id=<?php echo $value['id']; ?>" class="btn bg-danger text-white">Delete</a></td>
					</tr>
				</tbody>
				<?php 
					}
				 ?>
			</table>
		</form>
		<form method="POST">
			<button class="btn bg-danger text-white float-right" name="logout">Logout</button>
		</form>
	</div>
</body>
</html>