<?php 

	require_once('Connection.php');	  	

	if (!$user->isLoggedIn()) {
 		$user->redirect('index.php');
	}

	if (isset($_POST['logout'])) {
		$user->logout();
		$user->redirect('index.php');
	}

 ?>

<!doctype html>
<!-- If multi-language site, reconsider usage of html lang declaration here. -->
<html lang="en"> 
	<head>
		<meta charset="utf-8">
		<!-- Setting the viewport for Media Query -->
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<title>Messaging App | Home</title>
		<!-- Place favicon.ico in the root directory: mathiasbynens.be/notes/touch-icons -->
		<link rel="shortcut icon" href="favicon.ico" />	
		<!-- Adding reference to font awesome -->
		<link rel="stylesheet" href="assets/vendor/font/fontawesome-all.min.css">
		<!-- Default style-sheet is for 'media' type screen (color computer display).  -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link rel="stylesheet" media="screen" href="assets/css/style.css">
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	  	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	</head>
	<body>

		<div class="container">
			<!--Main Starts Here-->
			<main>
				<div class="home-form-div">
					<h3>Welcome To Messaging App</h3>
					<form method="POST">
						<table class="table table-bordered table-striped table-responsive-stack mt-4">
							<thead class="text-center">
								<tr>
									<th>Name</th>
									<th>Username</th>
									<th>Email</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<?php 
								require_once('Connection.php');	
								$result = $user->seeAllUsers($_SESSION['user_session']);
								foreach ($result as $value) {
							 ?>	
							<tbody class="text-center">
								<td><?php echo $value['name']; ?></td>
								<td><?php echo $value['username']; ?></td>
								<td><?php echo $value['email']; ?></td>
								<td></td>
								<td><a href="chat.php?id=<?php echo $value['id']; ?>" class="start_chat btn btn-primary text-white" name="startChat">Start Chat</a></td>
							</tbody>
							<?php 
								}
							 ?>	
						</table>
					</form>
				</div>
				<div>
					<form method="POST">
						<button class="logout" name="logout">Logout</button>
					</form>
				</div>
			</main>
			<!--Main Ends Here-->
		</div>
	</body>
</html>