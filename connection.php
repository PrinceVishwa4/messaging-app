<?php 

	$DB_HOST = "localhost";
	$DB_USER = "root";
	$DB_PASSWORD = "root";
	$DB_DATABASE = "messaging_app";

	try {
		$pdo = new PDO("mysql:host={$DB_HOST};dbname={$DB_DATABASE}", $DB_USER, $DB_PASSWORD);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch(PDOException $e) {
		echo $e->getMessage();
	}

 ?>