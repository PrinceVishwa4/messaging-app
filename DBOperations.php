<?php 

	abstract class DBOperations 
	{
		abstract public function login($username, $password);
		abstract public function isLoggedIn();
		abstract public function logout();

		public function redirect($url)
		{
			header('Location:'.$url);
		}
	}

	class User extends DBOperations
	{
		private $db;

		public function __construct($pdo)
		{
			$this->db = $pdo;
		}

		public function register($name, $username, $email, $password)
		{
			try {
				$new_password = password_hash($password, PASSWORD_DEFAULT);
				$stmt = $this->db->prepare("INSERT INTO user(name, username, email, password) VALUES (:name, :username, :email, :password)");
				$stmt->execute(['name' => $name, 'username' => $username, 'email' => $email, 'password' => $new_password]);
				return $stmt;
			} catch(PDOException $e) {
				echo $e->getMessage();
			}
		}

		public function login($username, $password)
		{
			try {
				$stmt = $this->db->prepare("SELECT * FROM user WHERE username = :username LIMIT 1");
				$stmt->execute(['username' => $username]);
				$userRow = $stmt->fetch(PDO::FETCH_ASSOC);

				if ($stmt->rowCount() > 0) {
					if (password_verify($password, $userRow['password'])) {
						$_SESSION['user_session'] = $userRow['id'];
						$_SESSION['user_name'] = $userRow['username'];
						return true;
					} else {
						return false;
					}
				}
			} catch(PDOException $e) {
				echo $e->getMessage();
			}
		}

		public function isLoggedIn()
		{
			if (isset($_SESSION['user_session'])) {
				return true;
			}
		}

		public function logout()
		{
			session_destroy();
			unset($_SESSION['user_session']);
			return true;
		}

		public function checkUsername($username)
		{
			try {
				$stmt = $this->db->prepare("SELECT username FROM user WHERE username=:username");
				$stmt->execute(['username' => $username]);
				$result = $stmt->fetch();
				return $result;
			} catch(PDOException $e) {
				echo $e->getMessage();
			}
		}

		public function checkEmail($email)
		{
			try {
				$stmt = $this->db->prepare("SELECT email FROM user WHERE email=:email");
				$stmt->execute(['email' => $email]);
				$result = $stmt->fetch();
				return $result;
			} catch(PDOException $e) {
				echo $e->getMessage();
			}
		}

		public function findUserWithName($name)
		{
			try {
				$stmt = $this->db->prepare("SELECT * FROM user WHERE name=:name");
				$stmt->execute(['name' => $name]);
				$result = $stmt->fetchAll();
				return $result;
			} catch(PDOException $e) {
				echo $e->getMessage();
			}
		}

		public function getDataWithId($id)
		{
			try {
				$stmt = $this->db->prepare("SELECT * FROM user WHERE id = :id");
				$stmt->execute(['id' => $id]);
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch(PDOException $e) {
				echo $e->getMessagez();
			}			
		}		

		public function InsertChatMessage($senderId, $receiverId, $chat_message)
		{
			try{
				$stmt = $this->db->prepare("INSERT INTO message(senderId, receiverId, chat_message) VALUES (:senderId, :receiverId, :chat_message)");
				$stmt->execute(['senderId' => $senderId, 'receiverId' => $receiverId, 'chat_message' => $chat_message]);
			} catch(PDOException $e) {
				echo $e->getMessage();
			}			
		}

		public function getAllMessages($senderId, $receiverId)
		{
			try {
				$stmt = $this->db->prepare("SELECT * FROM message WHERE senderId = :senderId AND receiverId = :receiverId");
				$stmt->execute(['senderId' => $senderId, 'receiverId' => $receiverId]);
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch(PDOException $e) {
				echo $e->getMessage();
			}
		}

		public function seeAllUsers($id)
		{
			try {
				$stmt = $this->db->prepare("SELECT * FROM user WHERE id != :id");
				$stmt->execute(['id' => $id]);
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch(PDOException $e) {
				echo $e->getMessage();
			}
		}

		public function getUsernameById($id)
		{
			try {
				$stmt = $this->db->prepare("SELECT username FROM user WHERE id = :id");
				$stmt->execute(['id' => $id]);
				$result = $stmt->fetch(PDO::FETCH_ASSOC);
				return $result;
			} catch(PDOException $e) {
				echo $e->getMessage();
			}
		}
	}

	class Admin extends DBOperations
	{
		private $db;

		public function __construct($pdo)
		{
			$this->db = $pdo;
		}		

		public function login($username, $password)
		{
			try {
				$stmt = $this->db->prepare("SELECT * FROM admin WHERE username=:username");
				$stmt->execute(['username' => $username]);
				$userRow = $stmt->fetch(PDO::FETCH_ASSOC);

				if ($stmt->rowCount() > 0) {
					if ($password == $userRow['password']) {
						$_SESSION['admin_session'] = $userRow['id'];
						return true;
					} else {
						return false;
					}
				}
			} catch(PDOException $e) {
				echo $e->getMessage();
			}
		}

		public function isLoggedIn()
		{
			if (isset($_SESSION['admin_session'])) {
				return true;
			}
		}

		public function logout()
		{
			session_destroy();
			unset($_SESSION['admin_session']);
			return true;
		}

		public function seeAllUsers()
		{
			try {
				$stmt = $this->db->prepare("SELECT * FROM user");
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $result;
			} catch(PDOException $e) {
				echo $e->getMessage();
			}
		}

		public function removeUser($id)
		{
			try {
				$stmt = $this->db->prepare("DELETE FROM user WHERE id = :id");
				$stmt->execute();
			} catch(PDOException $e) {
				echo $e->getMessage();
			}
		}		
	}

 ?>