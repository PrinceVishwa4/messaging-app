<?php 

    require_once('Connection.php');  

    if ($user->isLoggedIn()) {
        $user->redirect('home.php');
    }

    if (isset($_POST['login'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];

        echo "$username";
        echo "$password";

        if ($user->login($username, $password)) {
            $user->redirect('home.php');
        } else {
            $error = "Wrong Details!!!!";
        }
    }

    if (isset($_POST['register'])) {
        $user->redirect('register.php');
    }

?>

<!doctype html>
<!-- If multi-language site, reconsider usage of html lang declaration here. -->
<html lang="en"> 
	<head>
		<meta charset="utf-8">
		<!-- Setting the viewport for Media Query -->
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<title>Messaging App | Login</title>
		<!-- Place favicon.ico in the root directory: mathiasbynens.be/notes/touch-icons -->
		<link rel="shortcut icon" href="favicon.ico" />	
		<!-- Adding reference to font awesome -->
		<link rel="stylesheet" href="assets/vendor/font/fontawesome-all.min.css">
		<!-- Default style-sheet is for 'media' type screen (color computer display).  -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel="stylesheet" media="screen" href="assets/css/style.css">
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	</head>
	<body>

		<div class="container">
			<!--Main Starts Here-->
			<main>
				<div class="login-form-div">
					<h3>Login</h3>
					<form method="POST" class="cf">
					<?php 
						if (isset($error)) {
					?>	
			            <div class="alert alert-danger">
			                <span class="glyphicon glyphicon-warning-sign"></span>&nbsp; <?php echo $error; ?>
			            </div>
                    <?php  		
						}
					?>    
						<div>
							<label>Username : </label>
							<input type="text" name="username">
						</div>
						<div>
							<label>Password : </label>
							<input type="password" name="password">
						</div>
						<input type="submit" value="Login" name="login" class="login">
						<input type="submit" value="Register" name="register" class="register">
					</form>
			        <div class="text-center mt-3">
			        	<a href="adminLogin.php" class="text-center">I'm Admin</a>
			        </div>
				</div>
			</main>
			<!--Main Ends Here-->
		</div>
	</body>
</html>