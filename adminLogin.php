<?php 

    require_once('Connection.php');

    if ($admin->isLoggedIn()) {
        $admin->redirect('allUsers.php');
    }

    if (isset($_POST['login'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];
      
        if ($admin->login($username, $password)) {
            $admin->redirect('allUsers.php');
        } else {
            $error = "Wrong Details!!!!";
        }
    }

    if (isset($_POST['back'])) {
        $admin->redirect('index.php');
    }

?>

<!DOCTYPE html>
<html>
<head>
  <title>Messaging App | Admin Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
  <br>
  <div class="col-lg-6 m-auto">
    <div class="card">
      <h2 class="card-header bg-dark text-white text-center">Admin Login Page</h2>
      <form method="POST">
        <?php
            if(isset($error)) {
        ?>
            <div class="alert alert-danger">
                <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?>
            </div>
        <?php
            }   
        ?>    
        <br>
        <label>Username : </label>
        <input type="text" name="username" class="form-control">
        <br>
        <label>Password : </label>
        <input type="password" name="password" class="form-control">
        <br>
        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" id="defaultUnchecked" name="checkbox">
        </div>
        <br>
        <div class="btn-group d-flex">
          <a href="index.php" class="btn bg-success text-white" name="back">Back</a>
          <button type="submit" name="login" class="btn bg-primary text-white">Login</button>
        </div>
      </form>
    </div>
  </div>

</body>
</html>