<?php 

	require_once('Connection.php');
	$friendId = $_GET['id'];

	if (isset($_POST['sendMessage'])) {
		$message = $_POST['chatText'];
		$user->InsertChatMessage($_SESSION['user_session'], $friendId, $message);
	}

	if (isset($_POST['logout'])) {
		$user->logout();
		$user->redirect('index.php');
	}

 ?>

<!doctype html>
<!-- If multi-language site, reconsider usage of html lang declaration here. -->
<html lang="en"> 
	<head>
		<meta charset="utf-8">
		<!-- Setting the viewport for Media Query -->
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<title>Messaging App | Chat</title>
		<!-- Place favicon.ico in the root directory: mathiasbynens.be/notes/touch-icons -->
		<link rel="shortcut icon" href="favicon.ico" />	
		<!-- Adding reference to font awesome -->
		<link rel="stylesheet" href="assets/vendor/font/fontawesome-all.min.css">
		<!-- Default style-sheet is for 'media' type screen (color computer display).  -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel="stylesheet" media="screen" href="assets/css/style.css">
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	</head>
	<body>

		<div class="container">
			<!--Main Starts Here-->
			<main>
				<div class="chatting-form-div">
				<?php 
					require_once('Connection.php');
					$result = $user->getDataWithId($friendId);
					foreach ($result as $value) {
				?>	
					<h3>Chatting With <?php echo $value['username']; ?></h3>
				<?php 
					}
				 ?>	
					<div id="messageArea" class="messageArea">
						<?php 
							require_once('Connection.php');
							$result = $user->getAllMessages($_SESSION['user_session'], $friendId);
							foreach ($result as $value) {
								$subresultOfSender = $user->getUsernameById($value['senderId']);
								$subresultOfReceiver = $user->getUsernameById($value['receiverId']);
								echo $subresultOfSender['username']." : ".$value['chat_message']."<br>";
								echo $subresultOfReceiver['username']." : ".$value['chat_message']."<br>";
								break;

								// if ($value['senderId'] == $_SESSION['user_session']) {
								// 	echo "You : ".$value['chat_message']."<br>";
								// 	// break;
								// } else {
								// 	echo $subresult['username']." : ".$value['chat_message']."<br>";
								// 	// break;
								// }
							}
						 ?>
					</div>
					<form method="POST" class="cf">
						<textarea name="chatText" placeholder="Enter Message" id="chatText" class="message"></textarea>
						<input type="submit" name="sendMessage" class="sendMessage" value="Send" id="sendMessage">				
					</form>
				</div>
				<div>
					<form method="POST">
						<button class="logout" name="logout">Logout</button>
					</form>
				</div>
			</main>
			<!--Main Ends Here-->
		</div>
	</body>
</html>