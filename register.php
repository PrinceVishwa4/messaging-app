<?php 

	require_once('Connection.php');

	if ($user->isLoggedIn()) {
		$user->redirect('home.php');
	}

	if (isset($_POST['register'])) {
		$name = $_POST['name'];
		$username = trim($_POST['username']);
		$email = $_POST['email'];
		$password = $_POST['password'];

		if ($name == "") {
			$errors[] = "Please Provide Name";
		} else if ($username == "") {
			$errors[] = "Please Provide Username";
		} else if ($email == "") {
			$errors[] = "Please Provide Email ID";
		} else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$errors[] = "Please Provide Proper Email ID";
		} else if ($password == "") {
			$errors[] = "Please Provide Password";
		} else if (strlen($password) < 8) {
			$errors[] = "Password Length Should Be Greater Than 8 Characters";
		} else {
			try {
				$checkingUsername = $user->checkUsername($username);
				$checkingEmail = $user->checkEmail($email);

				if ($checkingUsername['username'] == $username) {
					$errors[] = "Username Already Exist";
				} else {
					if ($checkingEmail['email'] == $email) {
						$errors[] = "Email Already Exist";
					} else {
						if ($user->register($name, $username, $email, $password)) {
							$user->redirect('home.php');
						}
					}
				}
			} catch(PDOException $e) {
				echo $e->getMessage();
			}
		}
	}

?>

<!doctype html>
<!-- If multi-language site, reconsider usage of html lang declaration here. -->
<html lang="en"> 
	<head>
		<meta charset="utf-8">
		<!-- Setting the viewport for Media Query -->
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<title>Messaging App | Register</title>
		<!-- Place favicon.ico in the root directory: mathiasbynens.be/notes/touch-icons -->
		<link rel="shortcut icon" href="favicon.ico" />	
		<!-- Adding reference to font awesome -->
		<link rel="stylesheet" href="assets/vendor/font/fontawesome-all.min.css">
		<!-- Default style-sheet is for 'media' type screen (color computer display).  -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel="stylesheet" media="screen" href="assets/css/style.css">
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	</head>
	<body>

		<div class="container">
			<!--Main Starts Here-->
			<main>
				<div class="register-form-div">
					<h3>Register</h3>
					<form method="POST" class="cf">
					<?php 
						if (isset($errors)) {
							foreach ($errors as $error) {
					?>						
						<div class="alert alert-danger">
                     		<span class="glyphicon glyphicon-warning-sign"></span>&nbsp; <?php echo $error; ?>
                  		</div>
                    <?php  		
							}
						}
					?>                   		
						<div>
							<label>Name : </label>
							<input type="text" name="name">
						</div>
						<div>
							<label>Username : </label>
							<input type="text" name="username">
						</div>
						<div>
							<label>Email : </label>
							<input type="email" name="email">
						</div>
						<div>
							<label>Password : </label>
							<input type="password" name="password">
						</div>
						<input type="submit" value="Register" name="register" class="register">
					</form>
				</div>
			</main>
			<!--Main Ends Here-->
		</div>
	</body>
</html>